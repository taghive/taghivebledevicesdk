package com.tag_hive.taghivedevice;

import android.Manifest;
import android.app.Activity;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.SparseIntArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.tag_hive.taghive_device_sdk.TagHiveBluetoothAndroidDeviceSource;
import com.tag_hive.taghive_device_sdk.TagHiveBluetoothAndroidDeviceSourceImpl;
import com.tag_hive.taghive_device_sdk.TagHiveRadixHelper;
import com.tag_hive.taghive_device_sdk.TagHiveRadixHelperImpl;
import com.tag_hive.taghive_device_sdk.TagHiveTagDeviceRepository;
import com.tag_hive.taghive_device_sdk.TagHiveTagDeviceRepositoryImpl;
import com.trello.rxlifecycle2.android.FragmentEvent;
import com.trello.rxlifecycle2.components.support.RxFragment;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;

/**
 * A placeholder fragment containing a simple view.
 */
@SuppressWarnings({"FieldCanBeLocal", "unused"})
public class MainActivityFragment extends RxFragment {
    private static final String TAG = MainActivityFragment.class.getSimpleName();

    @BindView(R.id.mSimpleTextView1) TextView mSimpleTextView1;
    @BindView(R.id.mSimpleTextView2) TextView mSimpleTextView2;
    @BindView(R.id.mSimpleImageView1) ImageView mSimpleImageView1;
    @BindView(R.id.mNumIndicator) TextView mNumIndicator;

    private TagHiveTagDeviceRepository mTagHiveTagDeviceRepository;
    private SoundPool mSoundPool;
    private SparseIntArray mSoundMap = new SparseIntArray();
    private SparseIntArray mLoadCompleteSoundMap = new SparseIntArray();

    private static int[] SOUND_EFFECTS = {
            R.raw.se_01, R.raw.se_02, R.raw.se_03, R.raw.se_04, R.raw.se_05,
            R.raw.se_06, R.raw.se_07, R.raw.se_08, R.raw.se_09, R.raw.se_10,
            R.raw.se_11, R.raw.se_12
    };
    private boolean mPlayed;
    private Handler mHandler = new Handler();

    public MainActivityFragment() {}

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        ButterKnife.bind(this, view);
        TagHiveRadixHelper tagHiveRadixHelper = new TagHiveRadixHelperImpl();
        BluetoothManager bluetoothManager =
                (BluetoothManager) Objects.requireNonNull(getActivity())
                        .getSystemService(Context.BLUETOOTH_SERVICE);
        TagHiveBluetoothAndroidDeviceSource tagHiveBluetoothAndroidDeviceSource =
                new TagHiveBluetoothAndroidDeviceSourceImpl(bluetoothManager, tagHiveRadixHelper);
        mTagHiveTagDeviceRepository =
                new TagHiveTagDeviceRepositoryImpl(tagHiveBluetoothAndroidDeviceSource
                        , tagHiveRadixHelper);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            SoundPool.Builder builder = new SoundPool.Builder();
            AudioAttributes.Builder audioAttrBuilder = new AudioAttributes.Builder();
            audioAttrBuilder.setUsage(AudioAttributes.USAGE_GAME);
            audioAttrBuilder.setContentType(AudioAttributes.CONTENT_TYPE_UNKNOWN);
            builder.setAudioAttributes(audioAttrBuilder.build());
            builder.setMaxStreams(10);
            mSoundPool = builder.build();
        } else {
            mSoundPool = new SoundPool(10, AudioManager.STREAM_MUSIC, 0);
        }

        mSoundPool.setOnLoadCompleteListener((soundPool1, sampleId, status) -> {
            if (status == 0) {
                int sound = mSoundMap.get(sampleId);
                mLoadCompleteSoundMap.put(sound, sampleId);
            }
        });

        Context ctx = getContext();

        int soundId;

        for (int soundEffect:SOUND_EFFECTS) {
            soundId = mSoundPool.load(ctx, soundEffect, 1);
            mSoundMap.put(soundId, soundEffect);
        }

        return view;
    }

    private static final String[] PERMISSIONS = new String[]{
            Manifest.permission.BLUETOOTH,
            Manifest.permission.BLUETOOTH_ADMIN,
            Manifest.permission.ACCESS_FINE_LOCATION
    };

    @Override
    public void onStart() {
        super.onStart();
        String[] blePermissions = getBlePermissions();
        String[] deniedBlePermissions = getDeniedPermissionName(blePermissions);
        if (deniedBlePermissions == null || deniedBlePermissions.length == 0) {
            startBleTask();
        } else {
            requestPermissions(getActivity(), deniedBlePermissions);
        }
    }

    private String[] getDeniedPermissionName(String[] requiredPermissions) {
        List<String> denied = new ArrayList<>();
        for (String permissionName : PERMISSIONS) {
            if (ContextCompat.checkSelfPermission(
                    Objects.requireNonNull(getActivity()), permissionName)
                    != PackageManager.PERMISSION_GRANTED) {
                denied.add(permissionName);
            }
        }

        int size = denied.size();

        return size == 0?null:
                denied.toArray(new String[size]);
    }

    private String[] getBlePermissions() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.Q) {
            return new String[]{PERMISSIONS[0],
                    PERMISSIONS[1],
                    PERMISSIONS[2]
            };
        } else {
            return new String[]{PERMISSIONS[0],
                    PERMISSIONS[1],
                    PERMISSIONS[2],
                    Manifest.permission.ACCESS_BACKGROUND_LOCATION
            };
        }
    }

    private void requestPermissions(Activity activity, String[] deniedBlePermissions) {
        ActivityCompat
                .requestPermissions(activity
                        , deniedBlePermissions
                        , MainActivity.CODE_REQUEST_DENIED_PERMISSION
                );
    }


    void startBleTask() {
        Disposable disposable = mTagHiveTagDeviceRepository
                .startObservingTagDetection()
                .compose(bindUntilEvent(FragmentEvent.STOP))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(tagHiveDevice -> {
                    int switchNum = -1;
                    Integer resourceId = null;
                    switch (tagHiveDevice.mSwitchEvent) {
                        case CHOICE_1:
                            resourceId = R.drawable.kong_food_pudding;
                            switchNum = 1;
                            break;
                        case CHOICE_2:
                            resourceId = R.drawable.kong_food_vegetable;
                            switchNum = 2;
                            break;
                        case CHOICE_3:
                            resourceId = R.drawable.kong_food_fish;
                            switchNum = 3;
                            break;
                        case CHOICE_4:
                            resourceId = R.drawable.kong_food_meat;
                            switchNum = 4;
                            break;
                        case CHOICE_5:
                            resourceId = R.drawable.kong_sink_soap;
                            switchNum = 5;
                            break;
                        case CHOICE_O:
                            resourceId = R.drawable.kong_sink_faucet;
                            switchNum = 6;
                            break;
                        case CHOICE_X:
                            resourceId = R.drawable.kong_sink_toothbrush;
                            switchNum = 7;
                            break;
                        case SPECIAL:
                            resourceId = R.drawable.kong_sink_left_molar;
                            switchNum = 8;
                            break;
                    }

                    handleSwitchNum(switchNum, resourceId);
                });
    }

    private void handleSwitchNum(int switchNum, Integer resourceId) {
        if (!mPlayed) {
            mPlayed = true;
            mHandler
                    .postDelayed(() -> mPlayed = false, 2000);

            int soundId = mLoadCompleteSoundMap.get(SOUND_EFFECTS[switchNum - 1]
                    , -1);

            if (soundId != -1) {
                if (mNumIndicator.getVisibility() != View.VISIBLE) {
                    mNumIndicator.setVisibility(View.VISIBLE);
                }
                mNumIndicator.setText(String.valueOf(switchNum));
                mSoundPool.play(soundId
                        , 0.5f
                        , 0.5f
                        , 1
                        , 0
                        , 1f
                );
                mSimpleImageView1.setImageResource(resourceId);
            }
        }
    }
}

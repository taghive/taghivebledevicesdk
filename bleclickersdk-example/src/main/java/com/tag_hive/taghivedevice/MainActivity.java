package com.tag_hive.taghivedevice;

import android.content.pm.PackageManager;
import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.trello.rxlifecycle2.components.support.RxAppCompatActivity;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends RxAppCompatActivity {
    public static final int CODE_REQUEST_DENIED_PERMISSION = 255;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(view -> Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show());
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions
            , @NonNull int[] grantResults) {
        if (requestCode == CODE_REQUEST_DENIED_PERMISSION) {
            List<String> deniedPermissionNames = new ArrayList<>();

            for (int i = 0; i < grantResults.length; i++) {
                if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                    deniedPermissionNames.add(permissions[i]);
                }
            }

            int size = deniedPermissionNames.size();

            handlePermissionResult(size == 0);

            if (size > 0) {
                Toast.makeText(getBaseContext(), R.string.needToAllowPermissions
                        , Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void handlePermissionResult(boolean hasDeniedPermissions) {
        MainActivityFragment frag = (MainActivityFragment) getSupportFragmentManager()
                .findFragmentById(R.id.fragment);
        if (frag != null) {
            frag.startBleTask();
        }
    }
}

# How to Implement TagHive BLE SDK

## Implementing Library File

### Download [AAR File](https://s3.amazonaws.com/tag-hive.com/sdk/bleclickersdk.aar)

### Import the AAR File into the project.

![tutorial-image1](./tutorial-res/tutorial1.png)

![tutorial-image2](./tutorial-res/tutorial2.png)

![tutorial-image3](./tutorial-res/tutorial3.png)

### in your app gradle file, apply following code `implementation project(':bleclickersdk')`

※ If you have finished up to here, you are now ready to implement the sdk.

### Reference bleclickersdk-example project detail implementation

* Setting up SDK in your fragment(or Activity)

```
@Override
public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                            Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragment_main, container, false);
    TagHiveRadixHelper tagHiveRadixHelper = new TagHiveRadixHelperImpl();
    BluetoothManager bluetoothManager =
            (BluetoothManager) Objects.requireNonNull(getActivity())
                    .getSystemService(Context.BLUETOOTH_SERVICE);
    TagHiveBluetoothAndroidDeviceSource tagHiveBluetoothAndroidDeviceSource =
            new TagHiveBluetoothAndroidDeviceSourceImpl(bluetoothManager, tagHiveRadixHelper);
    mTagHiveTagDeviceRepository =
            new TagHiveTagDeviceRepositoryImpl(tagHiveBluetoothAndroidDeviceSource
                    , tagHiveRadixHelper);

    return view;
}
```

* Implementing Permission Request Code inside Fragment(Or Activity)
```
@Override
public void onStart() {
    super.onStart();
    String[] blePermissions = getBlePermissions();
    String[] deniedBlePermissions = getDeniedPermissionName(blePermissions);
    if (deniedBlePermissions == null || deniedBlePermissions.length == 0) {
        startBleTask();
    } else {
        requestPermissions(getActivity(), deniedBlePermissions);
    }
}

private String[] getDeniedPermissionName(String[] requiredPermissions) {
    List<String> denied = new ArrayList<>();
    for (String permissionName : PERMISSIONS) {
        if (ContextCompat.checkSelfPermission(
                Objects.requireNonNull(getActivity()), permissionName)
                != PackageManager.PERMISSION_GRANTED) {
            denied.add(permissionName);
        }
    }

    int size = denied.size();

    return size == 0?null:
            denied.toArray(new String[size]);
}

private String[] getBlePermissions() {
    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.Q) {
        return new String[]{PERMISSIONS[0],
                PERMISSIONS[1],
                PERMISSIONS[2]
        };
    } else {
        return new String[]{PERMISSIONS[0],
                PERMISSIONS[1],
                PERMISSIONS[2],
                Manifest.permission.ACCESS_BACKGROUND_LOCATION
        };
    }
}

private void requestPermissions(Activity activity, String[] deniedBlePermissions) {
    ActivityCompat
            .requestPermissions(activity
                    , deniedBlePermissions
                    , MainActivity.CODE_REQUEST_DENIED_PERMISSION
            );
}
```

* Handling Permission Request Result in Activity

```
@Override
public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions
        , @NonNull int[] grantResults) {
    switch (requestCode) {
        case CODE_REQUEST_DENIED_PERMISSION:
            List<String> deniedPermissionNames = new ArrayList<>();

            for (int i = 0; i < grantResults.length; i++) {
                if ( grantResults[i] == PackageManager.PERMISSION_DENIED ) {
                    deniedPermissionNames.add( permissions[i] );
                }
            }

            int size = deniedPermissionNames.size();

            handlePermissionResult(size == 0);

            if ( size > 0 ) {
                Toast.makeText(getBaseContext(), R.string.needToAllowPermissions
                        , Toast.LENGTH_SHORT).show();
            }
            break;
    }
}

private void handlePermissionResult(boolean hasDeniedPermissions) {
    MainActivityFragment frag = (MainActivityFragment) getSupportFragmentManager()
            .findFragmentById(R.id.fragment);
    frag.startBleTask();
}
```

* Finally if all permission related task is done, implement codes to start handling signals from Clicker device.

```
void startBleTask() {
    Disposable disposable = mTagHiveTagDeviceRepository
            .startObservingTagDetection()
            .compose(bindUntilEvent(FragmentEvent.STOP))
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(tagHiveDevice -> {
                switch (tagHiveDevice.mSwitchEvent) {
                    case CHOICE_1:
                        Log.d("test", 1);
                        break;
                    case CHOICE_2:
                        Log.d("test", 2);
                        break;
                    case CHOICE_3:
                        Log.d("test", 3);
                        break;
                    case CHOICE_4:
                        Log.d("test", 4);
                        break;
                    case CHOICE_5:
                        Log.d("test", 5);
                        break;
                    case CHOICE_O:
                        Log.d("test", 6);
                        break;
                    case CHOICE_X:
                        Log.d("test", 7);
                        break;
                    case SPECIAL:
                        Log.d("test", 8);
                        break;
                }
            });
}
```

※ You should add dependencies for RXJava2 if you have not.

```
implementation 'io.reactivex.rxjava2:rxjava:2.2.10'
implementation 'io.reactivex.rxjava2:rxandroid:2.1.1'
implementation 'com.trello.rxlifecycle2:rxlifecycle:2.2.1'
implementation 'com.trello.rxlifecycle2:rxlifecycle-components:2.2.1'
```
